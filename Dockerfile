ARG NODEVER=10.14
FROM node:${NODEVER} as node

FROM selenium/standalone-chrome

WORKDIR /home/seluser

COPY --from=node /usr/local/bin /usr/local/bin
COPY --from=node /usr/local/lib/node_modules /usr/local/lib/node_modules

RUN sudo apt-get update && sudo apt-get install -y git

USER seluser

RUN git clone https://gitlab.com/rogaldh/prerender.git prerender && cd prerender && npm i

COPY --chown=seluser:seluser lib/server.js prerender/server.js

ARG NODEENV=production
ENV NODE_ENV=${NODEENV}

ARG SERVICEPORT=3030
ENV PORT ${SERVICEPORT}

EXPOSE ${SERVICEPORT}

CMD node prerender/server.js
