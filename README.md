> prerender.io container image

## How to

### Build
```bash
docker build -t service-prerender .
```

### Run
```bash
docker run -d -p 3030:3030 service-prerender
```
